package com.example.valentin.mtglifecounter;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private Button morePoison1;
    private Button lessPoison1;
    private Button lessPoison2;
    private Button morePoison2;
    private ImageButton moreLife1;
    private ImageButton lessLife1;
    private ImageButton moreLife2;
    private ImageButton lessLife2;
    private ImageButton dragLifeTo1;
    private ImageButton dragLifeTo2;
    private TextView player1Life;
    private TextView poisonPlayer1;
    private TextView player2Life;
    private TextView poisonPlayer2;
    private ImageButton reset;

    private int life1;
    private int life2;
    private int poison1;
    private int poison2;
    private int n1;

    public MainActivityFragment() {


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);



        morePoison1 = (Button) view.findViewById(R.id.morePoison1);
        lessPoison1 = (Button) view.findViewById(R.id.lessPoison1);
        lessPoison2 = (Button) view.findViewById(R.id.lessPoison2);
        morePoison2 = (Button) view.findViewById(R.id.morePoison2);
        moreLife1 = (ImageButton) view.findViewById(R.id.moreLife1);
        lessLife1 = (ImageButton) view.findViewById(R.id.lessLife1);
        moreLife2 = (ImageButton) view.findViewById(R.id.moreLife2);
        lessLife2 = (ImageButton) view.findViewById(R.id.lessLife2);
        dragLifeTo1 = (ImageButton) view.findViewById(R.id.dragLifeTo1);
        dragLifeTo2 = (ImageButton) view.findViewById(R.id.dragLifeTo2);
        player1Life = (TextView) view.findViewById(R.id.player1Life);
        poisonPlayer1 = (TextView) view.findViewById(R.id.poisonPlayer1);
        player2Life = (TextView) view.findViewById(R.id.player2Life);
        poisonPlayer2 = (TextView) view.findViewById(R.id.poisonPlayer2);
        reset = (ImageButton) view.findViewById(R.id.reset);


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.dragLifeTo1:
                        life1++;
                        life2--;
                        break;
                    case R.id.dragLifeTo2:
                        life2++;
                        life1--;
                        break;
                    case R.id.moreLife1:
                        life1++;
                        break;
                    case R.id.moreLife2:
                        life2++;
                        break;
                    case R.id.morePoison1:
                        poison1++;
                        break;
                    case R.id.morePoison2:
                        poison2++;
                        break;
                    case R.id.lessLife1:
                        life1--;
                        break;
                    case R.id.lessLife2:
                        life2--;
                        break;
                    case R.id.lessPoison1:
                        poison1--;
                        break;
                    case R.id.lessPoison2:
                        poison2--;
                        break;
                    case R.id.reset:
                        reset();
                        break;
                }
                updateViews();
            }
        };


        reset();

        dragLifeTo1.setOnClickListener(listener);
        dragLifeTo2.setOnClickListener(listener);
        moreLife1.setOnClickListener(listener);
        moreLife2.setOnClickListener(listener);
        lessLife1.setOnClickListener(listener);
        lessLife2.setOnClickListener(listener);
        morePoison1.setOnClickListener(listener);
        morePoison2.setOnClickListener(listener);
        lessPoison1.setOnClickListener(listener);
        lessPoison2.setOnClickListener(listener);
        player1Life.setOnClickListener(listener);
        player2Life.setOnClickListener(listener);
        poisonPlayer1.setOnClickListener(listener);
        poisonPlayer2.setOnClickListener(listener);
        reset.setOnClickListener(listener);

        return view;
    }

    private void reset() {
        poison1 = 0;
        poison2 = 0;
        life1 = 20;
        life2 = 20;
        
        updateViews();
    }

    private void updateViews() {
        player1Life.setText(String.valueOf(life1));
        player2Life.setText(String.valueOf(life2));
        poisonPlayer1.setText(String.valueOf(poison1));
        poisonPlayer2.setText(String.valueOf(poison2));
    }
}
